<?php
    error_reporting(E_ALL);
    require 'Simpy.class.php'; 
      
    $user = "";
    $pass = "";
      
    // new simpy class with the curl engine, replace the last parameter with 1 for snoopy
    $smp = new Simpy($user, $pass, 2);
      
    if (!$smp)
        die("Can't create Simpy class");
        
    // get all tags... default is 20, I've put 999 to get all my tags (around 150)        
    $tags = $smp->GetTags(999);
      
    // output the tags
    echo "<ol>";
    foreach ($tags as $tag)
        echo "<li>".$tag['tag']." - Count: ".$tag['count']."</li>";
    echo "</ol>";
?>