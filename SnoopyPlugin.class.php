<?php
    require "Snoopy.class.php";
    
    class SnoopyPlugin
    {
        var $uName;
        var $uPass;
        
        var $sObj;
        
        function SnoopyPlugin($username, $password)
        {
			$this->uName = $username;
			$this->uPass = $password;
		
			$this->sObj = new Snoopy();
		    
            if(!$this->sObj)
                return null;
            
            $this->sObj->user = $username;
            $this->sObj->pass = $password;
            $this->sObj->maxredirs = 5;
            $this->sObj->agent = "SimpyAPI for PHP";
        }
        
        function GetTags($limit)
        {
            if ($limit == null || !is_numeric($limit))
			    $this->sObj->fetch("http://www.simpy.com/simpy/api/rest/GetTags.do");
            else
                $this->sObj->fetch("http://www.simpy.com/simpy/api/rest/GetTags.do?limit=$limit");
            return $this->sObj->results;
        }
        
        function Search($query)
        {
            $url = "http://www.simpy.com/simpy/api/rest/GetLinks.do?q=".urlencode($query);
			$this->sObj->fetch($url);
            
            return $this->sObj->results;
        }
        
        function SearchEx($query, $params)
        {
            if (sizeof($params) == 0 || $params == null)
                return $this->Search($query);
            else {
                $q = "q=".urlencode($query)."&";
                
                foreach ($params as $param=>$val)
                    $q .= $param."=".urlencode($val)."&";
                
                $q[strlen($q)-1] = "";
            }
            $url = "http://www.simpy.com/simpy/api/rest/GetLinks.do?$q";
			$this->sObj->fetch($url);
            
            return $this->sObj->results;
        }
        
        function GetLinks($params)
        {
            if (sizeof($params) == 0 || $params == null)
                $url = "http://www.simpy.com/simpy/api/rest/GetLinks.do";
			else {
                $q = "";
                foreach ($params as $param=>$val)
                    $q .= $param."=".urlencode($val)."&";
                
                $q[strlen($q)-1] = "";
                $url = "http://www.simpy.com/simpy/api/rest/GetLinks.do?".$q;
            }
            
			$this->sObj->fetch($url);
            
            return $this->sObj->results;
        }
        
        function GetAllLinks() {
   			$url = "http://www.simpy.com/simpy/api/rest/GetLinks.do?limit=1000000";
			
			$this->sObj->fetch($url);
            echo $this->sObj->response_code;
            
            
            return $this->sObj->results;
        }
    }  
?>