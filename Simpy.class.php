<?php
    require "SimpleParser.class.php";
    
    /* plugins for http retrieval */
    require "CurlPlugin.class.php";
    require "SnoopyPlugin.class.php";
    
	class Simpy 
	{
		var $uName = "";
		var $uPass = "";
		
		var $hObj;
        var $sParser;
		
		function Simpy($username, $password, $engine = 2) 
		{
			$this->uName = $username;
			$this->uPass = $password;
		
            if ($engine == 1)
                $this->hObj = new CurlPlugin($username, $password);
		    else
                $this->hObj = new SnoopyPlugin($username, $password);
        
            $this->sParser = new SimpleParser;
        }
		
		function GetTags($limit=null)
		{
            $xml = $this->hObj->GetTags($limit);
            			
			if ($xml)
				return $this->_processTags($xml);
			else
				return null;			
		}
        
        function Search($query)
        {
            $xml = $this->hObj->Search($query);
            		
			if ($xml)
			    return $this->_processLinks($xml);
			else
				return null;			
        }
		
        function SearchEx($query, $params)
        {
            $xml = $this->hObj->Search($query);
            		
			if ($xml)
			    return $this->_processLinks($xml);
			else
				return null;			
        }
		
        function GetLinks($params = null)
		{
            $xml = $this->hObj->GetLinks($params);
            			
			if ($xml)
				return $this->_processLinks($xml);
			else
				return null;			
		}
		
		function GetAllLinks()
		{
            $xml = $this->hObj->GetAllLinks();
            			
			if ($xml)
				return $this->_processLinks($xml);
			else
				return null;			
		}
		
		function _processTags($xml_data)
		{
            $xml_data = str_replace("<tag=", "<tag tag=", $xml_data);
			$tagdata = $this->sParser->GetXMLTree($xml_data);
			
			$tags = array();

			foreach ($tagdata['tags']['tag'] as $tag)
				array_push($tags, array("tag" => $tag['name'], "count" => $tag['count']));
			
			return $tags;
		}
		
		function _processLinks($xml_data)
		{
			$linkdata = $this->sParser->GetXMLTree($xml_data);
			
			$links = array();
			
			foreach ($linkdata['links']['link'] as $link)
				array_push($links, array("url"=>$link['url'], "title"=>$link['title'], "note"=>$link['note'], 
                "addDate"=>$link['addDate'], "modDate"=>$link['modDate'], "tags"=>$link['tags']['tag']));
				
			return $links;
        }
	}
?>