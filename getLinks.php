<?php

        $output = "";
        
        $links = simpyapi_links_get_by_tag($tag);
        
        if (sizeof($links) == 0) {
            $output = t("There are no bookmarks tagged with ").$tag; 
        }
        else {
            $rtags = simpyapi_tags_get_related($tag);
        
            if (sizeof($rtags) > 0) {
                $output .= "<p>";
                $output .= t("Related tags: ");
            
                foreach ($rtags as $rtag)
                    $output .= "<a href='simpy/".$rtag."'>".$rtag."</a>  ";  
            
                $output .= "</p>";
            }
        
            foreach ($links as $link) {
                $output .= "<p><strong><a target='_blank' href='".$link['url']."'>".$link['title']."</a></strong><br />";
                $output .= t('Added on ').$link['addDate'].".<br />";
                
                if ($link['note'] != "")
                    $output .= "<em>".$link['note']."</em><br />";
            
                $tags = simpyapi_tags_get_by_link($link['id']);
            
                if (sizeof($tags) != 0) {
                    $output .= t('Simpy tags: ');
          
                    foreach ($tags as $tag)
                        $output .= "<a href='simpy/".$tag."'>".$tag."</a>  "; 
                }
            
                $output .= "</p>";
            }    
        }
        
    return $output;
?>