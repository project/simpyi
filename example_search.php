<?php
    error_reporting(E_ALL);
    require 'Simpy.class.php'; 
      
    $user = "";
    $pass = "";
      
    // new simpy class with the curl engine, replace the last parameter with 1 for snoopy
    $smp = new Simpy($user, $pass, 2);
      
    if (!$smp)
        die("Can't create Simpy class");
        
    // search some stuff
    $links = $smp->Search("net");
      
    // output the search results
    if ($links) {
        echo "<ol>";
        foreach ($links as $link) {
            echo "<li><a href='".$link['url']."'>".$link['title']."</a>";
            if ($link['note'])
                echo "<em> Note: ".$link['note']."</em>";
                echo " Added on: ".$link['addDate'];  
                echo " Modified on: ".$link['modDate']; 
                if (count($link['tags'])) {
                    echo " Tags:";
                    foreach ($link['tags'] as $tag)
                        echo "<strong>$tag</strong>, ";
                }
            echo "</li>";
        }
        echo "</ol>";
    }
    else
        echo "No links found.";
?>