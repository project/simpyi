/* simpy.js */
var simpyEffect;

function addEvent(object, type, handler)
{
	if (object.addEventListener) {
		object.addEventListener(type, handler, false);
	} else if (object.attachEvent) {
		object.attachEvent(['on',type].join(''),handler);
	} else {
		object[['on',type].join('')] = handler;
	}
} 

addEvent(window,"load",function() {
    simpyEffect = new fx.Combo('simpyLinks', {opacity:true, duration:600});
}); 

function simpyGetLinks(tag) {
    var dv = document.getElementById('simpyLinks');
    if (!dv) {
        alert("Something went wrong.");
        return;
    }
    
    dv.innerHTML = "<p class='simpyCenter'><img src='/modules/simpy/loading.gif' alt='Loading'/></p>";
    
    new ajax ('simpy_ajax', {
        postBody: 'tag='+tag, 
        onComplete: myFunction
    });
    
    return false;
}

//this will be called on request completion. 
//The request object will be passed by moo.ajax as default.
function myFunction(request){
    simpyEffect.hide();
    
    var dv = document.getElementById('simpyLinks');
    if (!dv) {
        alert("Something went wrong.");
        return;
    }
    
    dv.innerHTML = request.responseText;
    
    simpyEffect.toggle();
}