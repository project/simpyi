<?php
    class CurlPlugin
    {
        var $uName;
        var $uPass;
        
        var $cObj;
        
        function CurlPlugin($username, $password)
        {
			$this->uName = $username;
			$this->uPass = $password;
		
			$this->cObj = curl_init();
		    
            if(!$this->cObj)
                return null;
                   
			curl_setopt($this->cObj, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($this->cObj, CURLOPT_HTTPAUTH, CURLAUTH_ANY); 
			curl_setopt($this->cObj, CURLOPT_USERPWD, "$username:$password");	
			curl_setopt($this->cObj, CURLOPT_FOLLOWLOCATION, 1);
        }
        
        function GetTags($limit)
        {
            if ($limit == null || !is_numeric($limit))
			    curl_setopt($this->cObj, CURLOPT_URL, "http://www.simpy.com/simpy/api/rest/GetTags.do");
			else
			    curl_setopt($this->cObj, CURLOPT_URL, "http://www.simpy.com/simpy/api/rest/GetTags.do?limit=$limit");
            
			$xml = curl_exec($this->cObj);
			$code = curl_getinfo($this->cObj, CURLINFO_HTTP_CODE);
			
			if ($code == 0 || $code == 200)
				return $xml;
			else
				return null;
        }
        
        function Search($query)
        {
            $url = "http://www.simpy.com/simpy/api/rest/GetLinks.do?q=".urlencode($query);
            curl_setopt($this->cObj, CURLOPT_URL, $url);
			$xml = curl_exec($this->cObj);
			$code = curl_getinfo($this->cObj, CURLINFO_HTTP_CODE);
			
			if ($code == 0 || $code == 200)
				return $xml;
			else
				return null;			
        }
        
        function SearchEx($query, $params)
        {
            if (sizeof($params) == 0 || $params == null)
                return $this->Search($query);
            else {
                $q = "q=".urlencode($query)."&";
                
                foreach ($params as $param=>$val)
                    $q .= $param."=".urlencode($val)."&";
                
                $q[strlen($q)-1] = "";
            }
            $url = "http://www.simpy.com/simpy/api/rest/GetLinks.do?$q";
            curl_setopt($this->cObj, CURLOPT_URL, $url);
			$xml = curl_exec($this->cObj);
			$code = curl_getinfo($this->cObj, CURLINFO_HTTP_CODE);
			
			if ($code == 0 || $code == 200)
				return $xml;
			else
				return null;			
        }
        
        function GetLinks($params)
        {
            if (sizeof($params) == 0 || $params == null)
                curl_setopt($this->cObj, CURLOPT_URL, "http://www.simpy.com/simpy/api/rest/GetLinks.do");
			else {
                $q = "";
                foreach ($params as $param=>$val)
                    $q .= $param."=".urlencode($val)."&";
                
                $q[strlen($q)-1] = "";
                curl_setopt($this->cObj, CURLOPT_URL, "http://www.simpy.com/simpy/api/rest/GetLinks.do?$q");
            }
            
			$xml = curl_exec($this->cObj);
			$code = curl_getinfo($this->cObj, CURLINFO_HTTP_CODE);
			
			if ($code == 0 || $code == 200)
				return $xml;
			else
				return null;			
        }
        
        function GetAllLinks() {
   			curl_setopt($this->cObj, CURLOPT_URL, "http://www.simpy.com/simpy/api/rest/GetLinks.do?limit=1000000");
			
			$xml = curl_exec($this->cObj);
			$code = curl_getinfo($this->cObj, CURLINFO_HTTP_CODE);
			
			if ($code == 0 || $code == 200)
				return $xml;
			else
				return null;			
        }
    }  
?>