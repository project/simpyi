<?php
function simpyapi_tag_exists($tag) {
    $tag = db_escape_string($tag);
    
    $result = db_query("select sid from simpy_tags where stag='$tag'");
    if (db_num_rows($result) > 0) {
        $ob = db_fetch_object($result);
        return $ob->sid;
    }        
    else
        return -1;
}

function simpyapi_tag_add($tag) {
    $tag = db_escape_string($tag);
    
    db_query("insert into simpy_tags (stag) values ('$tag')");
    
    return simpyapi_tag_exists($tag);
}

function simpyapi_tag_delete($tagid) {
    $tagid = db_escape_string($tagid);
    
    db_query("delete from simpy_tags where sid='$tagid'");
}

function simpyapi_link_exists($link) {
    $link = db_escape_string($link);
    
    $result = db_query("select id from simpy_links where url='$link'");
    if (db_num_rows($result) > 0) {
        $ob = db_fetch_object($result);
        return $ob->id;
    }        
    else
        return -1;
}

function simpyapi_link_add($title, $url, $note, $addd, $modd, $tags) {
    $title = db_escape_string($title);
    $url = db_escape_string($url);
    $note = db_escape_string($note);
    $addd = db_escape_string($addd);
    $modd = db_escape_string($modd);
    
    db_query("insert into simpy_links (title, url, note, addDate, modDate) values ('$title', '$url', '$note', '$addd', '$modd')");
    
    $lid = simpyapi_link_exists($url);
    
    foreach ($tags as $tag)
    {
        $tid = simpyapi_tag_exists($tag);
        
        if ($tid == -1) 
            $tid = simpyapi_tag_add($tag);
            
        db_query("insert into simpy_rel (tid, lid) values ($tid, $lid)");
    }
}

function simpyapi_link_update($title, $url, $note, $addd, $modd, $tags) {
    $lid = simpyapi_link_exists($url);
    
    $title = db_escape_string($title);
    $url = db_escape_string($url);
    $note = db_escape_string($note);
    $addd = db_escape_string($addd);
    $modd = db_escape_string($modd);
    
    if ($lid == -1)
        return;
        
    db_query("update simpy_links set title='$title', note='$note', addDate='$addd', modDate='$modd' where id='$lid'");
    
    foreach ($tags as $tag) {
        $tid = simpyapi_tag_exists($tag);
        
        if ($tid == -1) 
            $tid = simpyapi_tag_add($tag);
            
        if (!simpyapi_rel_exists($tid, $lid))
            simpyapi_rel_add($tid, $lid);
    }
}

function simpyapi_link_delete($linkid) {
    $linkid = db_escape_string($linkid);
    
    db_query("delete from simpy_links where id='$linkid'");
    db_query("delete from simpy_rel where lid='$linkid'");
}

function simpyapi_rel_exists($tid, $lid) {
    $tid = db_escape_string($tid);
    $lid = db_escape_string($lid);
    
    $result = db_query("select id from simpy_rel where tid='$tid' and lid='$lid'");
    if (db_num_rows($result) > 0) 
        return true;
    else
        return false;
}

function simpyapi_rel_add($tid, $lid) {
    $tid = db_escape_string($tid);
    $lid = db_escape_string($lid);
    
    db_query("insert into simpy_rel (tid, lid) values ('$tid', '$lid')");
    
    return simpyapi_tag_exists($tag);
}

function simpyapi_tags_get($max = '') {
    if ($max == '')
        $result = db_query("select stag, count(id) as cnt from simpy_tags, simpy_rel where sid = tid group by stag");
    else
        $result = db_query("select stag, count(id) as cnt from simpy_tags, simpy_rel where sid = tid group by stag order by cnt desc limit 0, $max");
    
    $tags = array();
    
    while ($a = db_fetch_array($result))
        array_push($tags, array('tag'=>$a['stag'], 'count'=>$a['cnt']));

    sort($tags);
        
    return $tags;
}

function simpyapi_tags_get_by_link($lid) {
    $res1 = db_query("select stag from simpy_tags, simpy_rel where simpy_rel.lid='$lid' and simpy_rel.tid = sid");
    
    $tags = array();
    
    while ($a = db_fetch_array($res1)) {
        array_push($tags, $a['stag']);    
    }
    
    return $tags;
}

function simpyapi_links_get_by_tag($tag) {
    $tid = simpyapi_tag_exists($tag);
    
    if ($tid == -1)
        return null;
        
    $result = db_query("select sl.url, sl.title, sl.addDate, sl.id, sl.note from simpy_links sl, simpy_rel where simpy_rel.tid = '$tid' and simpy_rel.lid = sl.id");
    
    $links = array();
    
    while ($a = db_fetch_array($result)) {
        array_push($links, $a);
    }
        
    return $links;
}
  
function simpyapi_links_get_all() {
    $result = db_query("select * from simpy_links");
    
    $links = array();
    
    while ($a = db_fetch_array($result)) {
        array_push($links, $a);
    }
        
    return $links;
}

function simpyapi_tags_get_related($tag) {
    $tag = db_escape_string($tag);
    
    $result = db_query("SELECT pt.lid FROM simpy_rel pt, simpy_tags t WHERE pt.tid = t.sid AND t.stag='$tag'");
    while ($i = db_fetch_object($result)) {
        $insql .= $i->lid.",";
    }
    
    $insql = substr($insql, 0, strlen($insql)-1);
    
    $result = db_query("SELECT *, COUNT(pt.lid) AS count FROM simpy_rel pt, simpy_tags t WHERE pt.lid IN (".$insql.") AND t.sid = pt.tid GROUP BY pt.tid ORDER BY count DESC");
    
    $rtags = array();
    
    while ($ar = db_fetch_array($result)) {
        if ($ar['stag'] != $tag && $ar['count'] > 2)
            array_push($rtags, $ar['stag']);        
    }
    
    return $rtags;
}
  
?>